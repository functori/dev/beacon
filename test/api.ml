let count = ref 0

let test s =
  Format.printf "beacon %d received\n%S@." !count s;
  incr count;
  EzAPIServer.return_ok ""
[@@post {path="/test"; raw_output=[]; raw_input=[]}]

[@@@server 8080]
