let%meth test _app =
  let b = Beacon.send ~data:(`string "truc") "http://localhost:8080/test" in
  Ezjs_min.log "Beacon received %B" b

let () =
  let open Js_of_ocaml.Dom_events in
  ignore @@ listen Js_of_ocaml.Dom_html.document (Typ.make "visibilitychange") @@ fun t _ev ->
  if Ezjs_min.to_string (Ezjs_min.Unsafe.coerce t)##.visibilityState = "hidden" then
    Beacon.send ~data:(`string "some data") "/log"
  else true


[%%app {mount}]
