## Beacon Web API

```ocaml
open Ezjs_min
open Js_of_ocaml.Dom_events

let () =
  ignore @@ listen Dom_html.document (Typ.make "visibilitychange") @@ fun t _ev ->
  if to_string (Unsafe.coerce t)##.visibilityState = "hidden" then
    Beacon.send ~data:(`string "some data") "/log"
  else true
```
