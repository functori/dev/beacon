open Ezjs_min

class type navigator = object
  method sendBeacon: js_string t -> bool t meth
  method sendBeacon_buffer: js_string t -> Typed_array.arrayBuffer t -> bool t meth
  method sendBeacon_blob: js_string t -> File.blob t -> bool t meth
  method sendBeacon_dataview: js_string t -> Typed_array.dataView t -> bool t meth
  method sendBeacon_array: js_string t -> _ Typed_array.typedArray t -> bool t meth
  method sendBeacon_string: js_string t -> js_string t -> bool t meth
  method sendBeacon_form: js_string t -> Js_of_ocaml.Form.formData t -> bool t meth
  method sendBeacon_any: js_string t -> Unsafe.any -> bool t meth
end

type ('a, 'b) data = [
  | `buffer of Typed_array.arrayBuffer t
  | `blob of File.blob t
  | `dataview of Typed_array.dataView t
  | `array of ('a, 'b) Typed_array.typedArray t
  | `string of string
  | `form of Js_of_ocaml.Form.formData t
]

let send ?(data : [< _ data] option) url =
  let navigator : navigator t = Unsafe.global##.navigator in
  let url = string url in
  let b = match data with
    | None -> navigator##sendBeacon url
    | Some `buffer b -> navigator##sendBeacon_buffer url b
    | Some `blob b -> navigator##sendBeacon_blob url b
    | Some `dataview d -> navigator##sendBeacon_dataview url d
    | Some `array a -> navigator##sendBeacon_array url a
    | Some `string s -> navigator##sendBeacon_string url (string s)
    | Some `form f -> navigator##sendBeacon_form url f in
  to_bool b
